# Noisy Surfaces: A Fusion360 Add-In

Noisy Surfaces is a Fusion360 Add-In that allows users to add randomly generated noise to their bodies and surfaces for a more natural, rough look and feel. An installation guide and description of the plugin and its functionalities can be downloaded here: [manual.pdf](/resources/readme/manual.pdf)

![TrailerImage](/resources/readme/heads.png)

When creating and modelling 3D objects, it is important to have a variety of easy-to-use editing tools that allow for extensive modifications. The Noisy Surfaces plugin for Fusion360 offers just that: Numerous options for customizing surface noises and distorting any object in Fusion. By simply using an existing mesh or converting an object to a mesh, it is possibly to apply the plugin. Then it is possible to choose between five different filters, to create a turbulent, patterned-like, self-customised, or uneven surface. The result is a customised, optionally highly detailed, more natural looking 3D model, within a few clicks.

![TrailerImage](/resources/readme/valueNoisePlane.png)
![TrailerImage](/resources/readme/working.png)
![TrailerImage](/resources/readme/preview.png)
![TrailerImage](/resources/readme/newheads.png)

